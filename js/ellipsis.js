$(function(){
    var len = 27; // 超過n個字以"..."取代
    $(".feature__page section .card__title").each(function (i) {
      if ($(this).text().length > len) {
        $(this).attr("title", $(this).text());
        var text =
          $(this)
            .text()
            .substring(0, len - 1) + "...";
        $(this).text(text);
      }
    });
});


$(function () {
  if ($(window).width() <= 375) {
    var len = 24; // 超過n個字以"..."取代
    $(".feature__page section .card__title").each(function (i) {
      if ($(this).text().length > len) {
        $(this).attr("title", $(this).text());
        var text =
          $(this)
            .text()
            .substring(0, len - 1) + "...";
        $(this).text(text);
      }
    });
  }
});